<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Séries</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/380654a49c.js" crossorigin="anonymous"></script>
    <style>
        form {
            margin: 0;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-2 d-flex justify-content-between">
        <a class="navbar navbar-expand-lg" href="{{ route('listar_series') }}">Home</a>

        @auth
        <a href="/sair" class="text-danger">Sair</a>
        @endauth

        @guest()
        <a href="/entrar">Entrar</a>
        @endguest
   </nav>
    <div class="container">
        <div class="jumbotron">
           <h1>@yield('cabecalho')</h1>
        </div>
        <div>
            @yield('conteudo')
        </div>
    </div>
</body>
</html>