@extends('layout')

@section('cabecalho')
Adicionar Série
@endsection

@section('conteudo')
@include ('erros', ['errors' => $errors])
<form method="post">
    @csrf
    <div class="row">
        <div class="col col-8">
            <label for="nome">Serie</label>
            <input type="text" id="nome" class="form-control mb-2" name="nome">
        </div>
        <div class="col col-2">
            <label for="qtd_temporadas">Temporadas</label>
            <input type="number" id="qtd_temporadas" class="form-control mb-2" name="qtd_temporadas">
        </div>
        <div class="col col-2">
            <label for="ep_por_temporada">Ep. por temporada</label>
            <input type="number" id="ep_por_temporada" class="form-control mb-2" name="ep_por_temporada">
        </div>
    </div>

    <button class="btn btn-primary mt-2">Adicionar</button>
</form>
@endsection