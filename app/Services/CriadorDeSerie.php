<?php

namespace App\Services;

use App\Serie;
use Illuminate\Support\Facades\DB;

class CriadorDeSerie
{
    public function criarSerie(
        string $nomeSerie, 
        int $qtdTemporadas, 
        int $epPorTemporada
    ) : Serie
    {
        DB::beginTransaction();
        $serie = Serie::create(['nome' => $nomeSerie]);
        $this->criaTemporada($serie, $qtdTemporadas, $epPorTemporada);
        DB::commit();
        
        return $serie;
    }

    public function criaTemporada($serie, $qtdTemporadas, $epPorTemporada) : void
    {
        for ($i=1; $i <= $qtdTemporadas; $i++) { 
            $temporada = $serie->temporadas()->create(['numero' => $i]);
            
            $this->criaEpisodio($temporada, $epPorTemporada);
        }
    }

    public function criaEpisodio($temporada, $epPorTemporada) : void
    {
        for ($j=1; $j <= $epPorTemporada; $j++) {
            $temporada->episodios()->create(['numero' => $j]);
        }
    }

    
}