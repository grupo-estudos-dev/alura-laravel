<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\{Serie, Temporada, Episodio};

class RemovedorDeSerie
{
    public function removerSerie(int $serieId) : string
    {
        DB::beginTransaction();    

        $serie      = Serie::find($serieId);
        $nomeSerie  = $serie->nome;
        $this->removerTemporadas($serie);
        $serie->delete();

        DB::commit();
        
        return $nomeSerie;
    }

    public function removerTemporadas($serie) : void
    {
        $serie->temporadas->each(function (Temporada $temporada) {
            $this->removerEpisodio($temporada);            
            $temporada->delete();            
        });
    }

    public function removerEpisodio($temporada) : void 
    {
        $temporada->episodios->each(function (Episodio $episodio) {
            $episodio->delete();
        });
    }
} 
